#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract
import timeit
# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_less_than(self): # checks that rmse is within tolearable bounds
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        rmse = netflix_eval(r, w)[0]
        self.assertLess(rmse, 1.0)

        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861\n712610\n1772839\n1059319\n2380848\n548064")
        w = StringIO()
        rmse = netflix_eval(r, w)[0]
        self.assertLess(rmse, 1.0)
        
        r = StringIO("10031:\n548690\n2518265\n1575474\n2203195\n1578801\n2614194\n1224756\n2157060\n1528644")
        w = StringIO()
        rmse = netflix_eval(r, w)[0]
        self.assertLess(rmse, 1.0)
        
    def test_runtime(self): # Test the runtime is less than a minute
        r = open('RunNetflix.in', 'r')
        w = StringIO()
        runtime = netflix_eval(r, w)[1]
        self.assertLess(float(runtime), 60.0) # make sure that it runs in less than a minute

        r.close()
        
        r = StringIO("10:\n1952305\n1531863") # make sure that rmse is a positive number
        W = StringIO()
        rmse = netflix_eval(r, w)[0]
        self.assertGreater(rmse, 0.0)

        r = StringIO("10000:\n200206\n523108")
        w = StringIO()
        rmse = netflix_eval(r, w)[0]
        self.assertGreater(rmse, 0.0)

    def test_prediction(self):  # assert that the prediction is more accurate by making sure it is a float
        from Netflix import get_decade_average
        prediction = get_decade_average(2417853, 10040, 2004)
        assert type(prediction) == float
        
        self.assertGreater(prediction, 0.0) # make sure the prediction is a positive number
        
        assert len(str(prediction)) == 3 # make sure the prediction is truncated to one decimal point


        

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
